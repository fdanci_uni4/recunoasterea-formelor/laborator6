package ro.usv.rf;

import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author fdanci
 *
 */
public class DistanceUtils {

	private static double DIFFERENCE = 0.0000001;

	// Get sorted tree map -> duplicates will be incremented a little
	private static TreeMap<Double, String> getGradesTreeMap(String[][] learningSet) {
		TreeMap<Double, String> treeMap = new TreeMap<Double, String>();

		for (String[] row : learningSet) {
			if(treeMap.containsKey(Double.valueOf(row[0]))) {
				treeMap.put(Double.valueOf(row[0]) + (DIFFERENCE += 0.0000001), row[1]);
			} else {
				treeMap.put(Double.valueOf(row[0]), row[1]);
			}
		}

		return treeMap;
	}

	/**
	 * Get first closest k classes for the given grade.
	 * 
	 * @param learningSet
	 * @param k
	 * @param grade
	 * @return first k classes
	 */
	public static Map<Double, String> getKClassesForGrade(String[][] learningSet, int k, double grade){
		int count = k;
		Map<Double, String> map = getGradesTreeMap(learningSet);
		Map<Double, String> result = new TreeMap<>();
		
		for (Map.Entry<Double,String> entry : map.entrySet()) {
			String value = entry.getValue();
			Double key = entry.getKey();
			
			if(key >= grade && count-- > 0) {
				result.put(key, value);
			}
		}

		return result;
	}

}
