package ro.usv.rf;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author fdanci
 *
 */
public class GradeClass {

	Map<String, Integer> gradeClasses = new HashMap<>();

	public GradeClass(Map<Double, String> map) {
		// Add each class string -> keep track of how many of each class we have
		map.entrySet().stream().forEach(e -> add(e.getValue()));
	}

	private void add(String gradeClass) {
		if(gradeClasses.containsKey(gradeClass)) {
			gradeClasses.put(gradeClass, (gradeClasses.get(gradeClass) + 1));
		} else {
			gradeClasses.put(gradeClass, 1);
		}
	}

	/**
	 * Get grade class that occurs most often
	 * 
	 * @return the grade class
	 */
	public String[] getMaxGradeClass() {
		String key = Collections.max(gradeClasses.entrySet(), Comparator.comparingInt(
				Map.Entry::getValue)).getKey();

		Integer value = Collections.max(gradeClasses.entrySet(), Comparator.comparingInt(
				Map.Entry::getValue)).getValue();

		return new String[] {key, String.valueOf(value)};
	}
	
	public Map<String, Integer> getGradeClasses(){
		return gradeClasses;
	}
}
