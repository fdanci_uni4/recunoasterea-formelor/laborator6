package ro.usv.rf;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;
import java.util.TreeMap;

import org.junit.jupiter.api.Test;

class DistanceUtilsTest {

	@Test
	void testGetKClassesForGrade() {
		Map<Double, String> correctGradeClassTree = new TreeMap<>();
		correctGradeClassTree.put(6.7, "sufficinet");
		correctGradeClassTree.put(8.0, "good");

		int k = 2;
		double grade = 6.3;
		
		String[][] learningSetTest = {
				{"1.5", "insufficient"},
				{"1.7", "insufficient"},
				{"2.7", "insufficient"},
				{"6.7", "sufficinet"},
				{"8.0", "good"},
				{"9.6", "very good"}};

		Map<Double, String> testingGradeClassTree = DistanceUtils
				.getKClassesForGrade(learningSetTest, k, grade);
		
		assertEquals(correctGradeClassTree, testingGradeClassTree);
	}

}
