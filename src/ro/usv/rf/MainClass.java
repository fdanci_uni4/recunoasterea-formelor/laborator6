package ro.usv.rf;

import java.util.Map;

/**
 * 
 * @author fdanci
 *
 */
public class MainClass {

	public static void main(String[] args) {
		String[][] learningSet;

		try {
			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;
			System.out.println(String.format("The learning set has %s patters "
					+ "and %s features", numberOfPatterns, numberOfFeatures));
			
			double[] gradesArray = {3.80, 5.75, 6.25, 7.25, 8.5};
			int[] kArray = {1,3,5,7,9,13,17}; 
			// Temporarily store classes for each k and grade before outputting
			Map<Double, String> gradeClassTree;
			
			// For each k value
			for(int i = 0; i < kArray.length; i++) {
				int k = kArray[i];
				// For each grade value
				for(int j = 0; j < gradesArray.length; j++) {
					gradeClassTree = DistanceUtils.getKClassesForGrade(
							learningSet, k, gradesArray[j]);
					OutputUtil.printMap(gradeClassTree, k, gradesArray[j]);
				}
			}
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}

}
