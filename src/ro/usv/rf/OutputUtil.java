package ro.usv.rf;

import java.text.DecimalFormat;
import java.util.Map;

/**
 * 
 * @author fdanci
 *
 */
public class OutputUtil {

	/**
	 * Displays required output in console.
	 * 
	 * @param <K>
	 * @param <V>
	 * @param map
	 * @param k
	 * @param grade
	 */
	public static <K, V> void printMap(Map<K, V> map, int k, double grade) {
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		System.out.println("K:" + k + "  GRADE: " + grade);

		System.out.print("Nearest neghbour(s): ");
		map.entrySet().stream()
		.forEach(e -> System.out.print(decimalFormat.format(e.getKey()) + ", "));

		System.out.print("\nNearest neghbour(s) class: ");
		map.entrySet().stream()
		.forEach(e -> System.out.print(e.getValue() + ", "));

		GradeClass gradeClasses = new GradeClass((Map<Double, String>) map);
		String[] maxGradeClass = gradeClasses.getMaxGradeClass();

		Map<String, Integer> gradesMap = gradeClasses.getGradeClasses();

		System.out.print("\nSearched grade class: " + maxGradeClass[0] + " ( ");
		gradesMap.entrySet().stream().forEach(e -> System.out.print(e.getValue() 
				+ " " + e.getKey() + ", "));
		System.out.print(")");

		// NEW LINE
		System.out.println("\n");
	}

}
